#include<stdio.h>
#include<stdlib.h>
#include"ttef.c"

void cpmap(Map m, Map* cp){
    for (int i = 0; i < 16; ++i) {
        (cp->cfield)[i/4][i%4] = m.cfield[i/4][i%4];
        (cp-> field)[i/4][i%4] =  m.field[i/4][i%4];
    }
    cp->score = m.score;
}

void safeputtwo(Map* m){
    if(!contzero(*m)){return;}
    put_random_two(m);
}

int potmov5(Map mp){
    int i,j,k,l,m,ii = 0,mi = 0;    //to shut the comp up
    mp.score = 0;    //is this actually neccessary?
    int* dirs = calloc(4, sizeof(int));
    Map***** p = malloc(4*sizeof(Map****));
    for (i = 0; i < 4; ++i) {
        p[i] = malloc(4*sizeof(Map***));
        for (j = 0; j < 4; ++j) {
            p[i][j] = malloc(4*sizeof(Map**));
            for (k = 0; k < 4; ++k) {
                p[i][j][k] = malloc(4*sizeof(Map*));
                for (l = 0; l < 4; ++l) {
                    p[i][j][k][l] = malloc(4*sizeof(Map));
                    for (m = 0; m < 4; ++m) {
                        init_struct(&(p[i][j][k][l][m]));
                        cpmap(mp, &(p[i][j][k][l][m]));

                        safeputtwo(&p[i][j][k][l][m]);
                        mergetogether(&p[i][j][k][l][m], i);
                        safeputtwo(&p[i][j][k][l][m]);
                        mergetogether(&p[i][j][k][l][m], j);
                        safeputtwo(&p[i][j][k][l][m]);
                        mergetogether(&p[i][j][k][l][m], k);

                        dirs[i] += p[i][j][k][l][m].score;
                    }
                }
            }
        }
        if(dirs[i] > ii){ii = dirs[i];mi = i;}
    }
    return mi;
}

int potmov3(Map m){
    int i,j,k,ii = 0,mi = 0;    //to shut the comp up
    m.score = 0;    //is this actually neccessary?
    int* dirs = calloc(4, sizeof(int));
    Map*** p = malloc(4*sizeof(Map**));
    for (i = 0; i < 4; ++i) {
        p[i] = malloc(4*sizeof(Map*));
        for (j = 0; j < 4; ++j) {
            p[i][j] = malloc(4*sizeof(Map));
            for (k = 0; k < 4; ++k) {
                init_struct(&(p[i][j][k]));
                cpmap(m, &(p[i][j][k]));

                safeputtwo(&p[i][j][k]);
                mergetogether(&p[i][j][k], i);
                safeputtwo(&p[i][j][k]);
                mergetogether(&p[i][j][k], j);
                safeputtwo(&p[i][j][k]);
                mergetogether(&p[i][j][k], k);

                dirs[i] += p[i][j][k].score;
            }
        }
        if(dirs[i] > ii){ii = dirs[i];mi = i;}
    }
    return mi;
}

void playpotmovBest3(){
    Map m;
    int d;
    init_struct(&m);
    put_random_two(&m);
    while(contzero(m)){
        put_random_two(&m);
        d = potmov3(m);
        mergetogether(&m, d);
        printmap(m);
    }
}

void playpotmovBest5(){
    Map m;
    int d;
    init_struct(&m);
    put_random_two(&m);
    while(contzero(m)){
        put_random_two(&m);
        d = potmov5(m);
        mergetogether(&m, d);
        printmap(m);
    }
}

int main(int argc, char *argv[])
{
    init_rand();    
    playpotmovBest5();
    return 0;
}
