#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int** field;
    int** cfield;
    int score;
}Map;

int power(int b, int e){
    int r = 1;
    for (int i = 0; i < e; ++i) {
        r*=b;
    } return r;
}

void init_rand(){
    srand((unsigned) time(NULL));
}
void init_struct(Map* m){
    m->field = malloc(4*sizeof(int*));
    m->cfield = malloc(4*sizeof(int*));
    for(int i = 0; i < 4; ++i){
        m->field[i] = calloc(4,sizeof(int));
        m->cfield[i] = calloc(4,sizeof(int));
    }
    m->score = 0;
    srand((unsigned) time(NULL));
}

void kill_struct(Map m){
    for (int i = 0; i < 4; ++i) {
        free(m.field[i]);
        free(m.cfield[i]);
    }
    free(m.field);
    free(m.cfield);
}

int rnd(int ub){
    return rand() % (ub + 1);
}

void swmp(Map* m){
    int** a = m->field;
    m->field = m->cfield;
    m->cfield = a;
    for (int i = 0; i < 16; ++i) {
        m->cfield[i/4][i%4] = 0;
    }
}

void transpose(Map* m){
    for(int i = 0; i < 4; ++i){for(int j = 0; j < 4; ++j){
            m->cfield[j][i] = m->field[i][j];
        }
    } swmp(m);
}

void reverse(Map* m){
    for(int i = 0; i < 4; ++i){for(int j = 0; j < 4; ++j){
        m->cfield[i][j] = m->field[i][3-j];
    }}swmp(m);
}

void rmzeros(Map* m){
    int i,j,c;
        for (i = 0; i < 4; ++i){
            c = 0;
            for (j = 0; j < 4; ++j) {
            if(m->field[i][j]){
                m->cfield[i][c++] = m->field[i][j];
            } } }
    swmp(m);
}

void mergeleft(Map* m){
    int i,j;
    rmzeros(m);
    for (i = 0; i < 4; ++i) { for (j = 0; j < 3; ++j) {
        if((m->field[i][j] == m->field[i][j+1]) && m->field[i][j]){
            m->score += power(2,m->field[i][j]+1);
            m->field[i][j] += 1;
            m->field[i][j+1] = 0;
        } } }
    rmzeros(m);
}

void mergetogether(Map* m, int d){
    if(d == 0){mergeleft(m);}
    if(d == 2){
        reverse(m);
        mergeleft(m);
        reverse(m);
    }
    if(d == 3){
        transpose(m);
        mergeleft(m);
        transpose(m);
    }
    if(d == 1){
        transpose(m);
        reverse(m);
        mergeleft(m);
        reverse(m);
        transpose(m);
    }
}

void printmap(Map m){
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            printf("%d ", m.field[i][j]); 
        }
        printf("\n");
    }
}

void put_random_two(Map* m){
    int i,j;
    i = rnd(3);
    j = rnd(3);
    if(m->field[i][j] == 0){m->field[i][j] = 1;}else{put_random_two(m);}
}

int contzero(Map m){
    for (int i = 0; i < 16; ++i) {
        if(!m.field[i/4][i%4]){return 1;}
    }
    return 0;
}

void playttef(){
    Map m;
    int d;
    init_struct(&m);
    put_random_two(&m);
    put_random_two(&m);
    while(contzero(m)){
        d = getchar(); 
        if(d=='a'){mergetogether(&m, 0);
            if(contzero(m)){put_random_two(&m);}
        }
        if(d=='s'){mergetogether(&m, 1);
            if(contzero(m)){put_random_two(&m);}
        }
        if(d=='w'){mergetogether(&m, 3);
            if(contzero(m)){put_random_two(&m);}
        }
        if(d=='d'){mergetogether(&m, 2);
            if(contzero(m)){put_random_two(&m);}
        }
        printmap(m);
        printf("Score: %d\n",m.score);
    }
}


//int main(int argc, char *argv[])
//{
//    playttef();
//    return 0;
//}
