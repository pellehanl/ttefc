# TTEF

This is the reference implementation for TTEF, (2048) and its solving algorithms

Compile game with:
```gcc -o ttef ttef.c```

Compile algorithm (5 moves ahead) with:
```gcc -o ttef algs.c```
